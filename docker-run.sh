#!/bin/sh

docker run --rm -it -v ${PWD}/docker-root:/project -v ${HOME}/.ssh:/home/hcloud/.ssh -e HCLOUD_TOKEN=$(cat .token) $(cat .docker-image) $*
