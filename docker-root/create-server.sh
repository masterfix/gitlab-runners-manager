#!/bin/sh

for KEY_ID in $(hcloud ssh-key list -o 'noheader' -o 'columns=id')
do
	SSH_KEY_PARAMS="${SSH_KEY_PARAMS} --ssh-key $KEY_ID"
done

hcloud server create --image 'ubuntu-20.04' --datacenter 'nbg1-dc3' --type 'cx11' --name 'gitlab-runners-manager' $SSH_KEY_PARAMS --user-data-from-file _configs/cloud-config.yml