#!/bin/sh

IMAGE_NAME="gitlab-runners-manager"

docker build -t $IMAGE_NAME $* .

echo $IMAGE_NAME > .docker-image