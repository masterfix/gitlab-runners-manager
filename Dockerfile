FROM alpine
RUN apk add hcloud openssh-client
RUN adduser -D hcloud
USER hcloud
WORKDIR /project