
.docker-image: Dockerfile
	./docker-build.sh

docker: .docker-image
	./docker-run.sh /bin/sh

clean:
	./docker-clean.sh

ssh: .docker-image
	./docker-run.sh ./ssh.sh

.PHONY: docker clean ssh