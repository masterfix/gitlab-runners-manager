#!/bin/bash

touch .docker-image
IMAGE=$(cat .docker-image)

docker image inspect "$IMAGE" >/dev/null 2>/dev/null

if [ $? == 0 ]; then
    docker rmi "$IMAGE"
fi

rm -f .docker-image